import React, { useState } from "react";
import Dict from "./Dict";

const App = () => {
  const [felinos, setFelinos] = useState(Dict);

  const onChange = (entry, event) => {
    setFelinos(Object.assign({}, felinos, { [entry[0]]: !entry[1] }));
  };

  return (
    <div className="App">
      <Container
        title="Felinos"
        items={Object.entries(felinos).filter(f => f[1])}
        onClick={e => onChange(e)}
      />
      <Container
        title="No felinos"
        items={Object.entries(felinos).filter(f => !f[1])}
        onClick={e => onChange(e)}
      />
    </div>
  );
};

const Container = props => {
  return (
    <section>
      <h3>{`${props.title} (${props.items.length})`}</h3>
      {props.items.map((item, i) => (
        <button key={i} onClick={e => props.onClick(item, e)}>
          {item}
        </button>
      ))}
    </section>
  );
};

export default App;